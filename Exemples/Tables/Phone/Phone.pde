void setup() {
  Table table = loadTable("phone.csv","header");
  
  printContact(table);
  sortContact(table);
  println("---");
  printContact(table);
  saveTable(table,"sorteddata.csv");
}

void printContact(Table T){
  for (int i=0; i<T.getRowCount();++i){
        TableRow row = T.getRow(i);
        String ln = row.getString("ln");
        String fn = row.getString("fn");
        String tel = row.getString("tel");
        println (ln,fn,tel);
    }
}

void sortContact(Table T){
  // function bubblesort adapted
  int droite, i;
  for(droite=T.getRowCount()-1;droite>0;--droite)
    for(i=0;i<droite;++i){
      String ln1 = T.getString(i,"ln");
      String ln2 = T.getString(i+1,"ln");
      String fn1 = T.getString(i,"fn");
      String fn2 = T.getString(i+1,"fn");
      if (ln1.compareTo(ln2)>0 || (ln1.compareTo(ln2) == 0 && fn1.compareTo(fn2)>0)){
        String t;
        t = ln1; T.setString(i,"ln",ln2); T.setString(i+1,"ln",t);
        t = T.getString(i,"fn"); T.setString(i,"fn",T.getString(i+1,"fn")); T.setString(i+1,"fn",t);
        t = T.getString(i,"tel"); T.setString(i,"tel",T.getString(i+1,"tel")); T.setString(i+1,"tel",t);  
      } 
    }
}