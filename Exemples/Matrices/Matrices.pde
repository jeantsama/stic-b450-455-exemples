boolean[][] M = new boolean[10][10];

void setup() {
  size(100, 100);
}

void draw(){
    background(255);
    for (int i = 0; i < 10; i ++ )
      for (int j =0; j < 10; ++j)
          if (M[i][j]){
              fill(0);
              stroke(200);
              rect(i*10,j*10,10,10);
          }
}

void keyPressed(){
    for (int i = 0; i < 10; i ++ )
      for (int j =0; j < 10; ++j)
          M[i][j] = false;
}

void mousePressed() {
    M[int(mouseX/10)][int(mouseY/10)] = true;
}