void setup(){
  size(200,200);
}

void draw(){
  background(255);
  stroke(0);
  fill(175);
  translate(width/2,height/2); // Translate origin to center
  rotate(PI/4);
  rectMode(CENTER);
  rect(0,0,100,100);
}