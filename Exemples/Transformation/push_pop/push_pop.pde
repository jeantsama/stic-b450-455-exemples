float r=0.0;
void setup(){
  size(200,200);
}
void draw(){
  size(200,200); smooth(); rectMode(CENTER);
// Repère au centre de l'écran 
translate(width/2,height/2);
// Sauvegarde de A 
pushMatrix();
// Transformation B 
rotate(PI/4);
// Dessin du carré noir 
fill(0); rect(0,0,120,120);
// Restauration A
// A ce point-là, notre repère revient au centre de l'écran 
popMatrix();
// Dessin du carré blanc qui ne tient pas compte de la rotation 
fill(255);
rect(0,0,50,50);
}