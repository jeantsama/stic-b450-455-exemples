PImage img;

void setup() {
    size(500, 500);
    img = loadImage("Person.jpg");


    loadPixels();
    img.loadPixels();


    for (int x = 0; x < img.width; x += 2) {
        for (int y = 0; y < img.height; y += 2) {



            int loc = x+y*img.width;
            int dloc = x/2+(y/2)*img.width;
            float r = red(img.pixels[loc]);
            float g = green(img.pixels[loc]);
            float b = blue(img.pixels[loc]);
            pixels[dloc] = color(r, g, b);
            
            loc = x+1+y*img.width;
            dloc = x/2+250+y/2*img.width;
             r = red(img.pixels[loc]);
             g = green(img.pixels[loc]);
             b = blue(img.pixels[loc]);
            pixels[dloc] = color(r,g,b);
            
            loc = (x+1)+(y+1)*img.width;
             dloc = (x/2+250)+(y/2+250)*img.width;
             r = red(img.pixels[loc]);
             g = green(img.pixels[loc]);
             b = blue(img.pixels[loc]);
            pixels[dloc] = color(r,g,b);
            
            loc = x+(y+1)*img.width;
             dloc = x/2+(y/2+250)*img.width;
            r = red(img.pixels[loc]);
             g = green(img.pixels[loc]);
             b = blue(img.pixels[loc]);
            pixels[dloc] = color(r,g,b);
        }
    }
    updatePixels();
}
