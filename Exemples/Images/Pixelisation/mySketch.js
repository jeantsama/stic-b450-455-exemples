var img;

function preload() {
	// 500 x 500 px
	img = loadImage("CodingBild.jpg");
}

function setup() {
	createCanvas(500, 500);
	background(0);
}

function draw() {
	background(0);
	//image(img, 0, 0);
	
	img.loadPixels();

	//jeder 20te pixel
	for (var y = 0; y < img.height; y += 10) {
		for (var x = 0; x < img.width; x += 10) {
			
      //var pixel = img.pixels[(y*img.width+x)*4];
			
			var r = img.pixels[(y*img.width+x)*4];
			var g = img.pixels[(y*img.width+x)*4+1];
			var b = img.pixels[(y*img.width+x)*4+2];
			
			//fill(255);
			//text(pixel, x, y);
	
			
			fill(r, g, b);
			ellipse (x, y, 10, 10);

			
			
			/*push();
			fill (r, g, b);
			translate(x, y);
			pop();
			*/
		}
	}
}