PImage img; 

    void setup() {
        size(500, 500);
        img = loadImage("Person.jpg");
    }

    void draw() {
        background(0);
        image(img, 0, 0, width, height);
    }
