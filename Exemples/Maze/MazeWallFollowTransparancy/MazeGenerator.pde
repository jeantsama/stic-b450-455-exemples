import java.util.Collections;
import java.util.Arrays;
 
/*
 * recursive backtracking algorithm
 * shamelessly borrowed from the ruby at
 * http://weblog.jamisbuck.org/2010/12/27/maze-generation-recursive-backtracking
 */

public class MazeGenerator {
  private final int x;
  private final int[][] maze;
 
  public MazeGenerator(int x) {
    this.x = x;
    maze = new int[this.x][this.x];
    generateMaze(0, 0);
    
    
  }
  
  public boolean isInsideMaze(int i, int j){return i >= 0 && j >= 0 && i < this.x && j < this.x; };
  
  public boolean isDeadEnd (int i, int j)
  {   
    int a = 0;
   if(isNorthWall(i,j))a++;
   if(isSouthWall(i,j))a++;
   if(isEastWall(i,j)) a++;
   if(isWestWall(i,j)) a++;
   return a == 3;
  }
  public boolean isNorthWall(int i, int j){return isInsideMaze(i,j) && (maze[i][j] & 1) == 0;}
  public boolean isSouthWall(int i, int j){return isInsideMaze(i,j) && (maze[i][j] & 2) == 0;}
  public boolean isEastWall(int i, int j) {return isInsideMaze(i,j) && (maze[i][j] & 4) == 0;}
  public boolean isWestWall(int i, int j) {return isInsideMaze(i,j) && (maze[i][j] & 8) == 0;}
 
 public int startX(){return 0;}
 public int startY(){return 0;}

 public int endX(){return this.x-1;}
 public int endY(){return this.x-1;}
 
 public int Size(){return this.x;}
 
 public void display()
 {
       background(255);
      fill(0);
     int sb = width/this.x ;
    for (int i = 0; i < this.x; i++)
    {
      for (int j = 0; j < this.x; j++)
      {
        if(this.isNorthWall(i,j)) rect(i*sb,j*sb,sb,2);
        if(this.isWestWall(i,j)) rect(i*sb,j*sb,2,sb);
      
      } 
    }
      rect(0,width-2,width,2);
      rect(width-2,0,2,width);
     
//start and end
  fill(0,255,0);
  ellipse(sb/2,sb/2, sb/2,sb/2); 
  fill(255,0,0);
  ellipse(width-sb/2,width-sb/2, sb/2,sb/2); 

 }
 
  public void ASCIIdisplay() {
    for (int i = 0; i < this.x; i++) {
      // draw the north edge
      for (int j = 0; j < x; j++) {
        System.out.print((maze[j][i] & 1) == 0 ? "+---" : "+   ");
      }
      System.out.println("+");
      // draw the west edge
      for (int j = 0; j < x; j++) {
        System.out.print((maze[j][i] & 8) == 0 ? "|   " : "    ");
      }
      System.out.println("|");
    }
    // draw the bottom line
    for (int j = 0; j < x; j++) {
      System.out.print("+---");
    }
    System.out.println("+");
  }
 
  private void generateMaze(int cx, int cy) {
    DIR[] dirs = DIR.values();
    Collections.shuffle(Arrays.asList(dirs));
    for (DIR dir : dirs) {
      int nx = cx + dir.dx;
      int ny = cy + dir.dy;
      if (between(nx, x) && between(ny, this.x)
          && (maze[nx][ny] == 0)) {
        maze[cx][cy] |= dir.bit;
        maze[nx][ny] |= dir.opposite.bit;
        generateMaze(nx, ny);
      }
    }
  }
 
  private boolean between(int v, int upper) {
    return (v >= 0) && (v < upper);
  }
 
 
/* 
  public static void main(String[] args) {
    int x = args.length >= 1 ? (Integer.parseInt(args[0])) : 8;
    int y = args.length == 2 ? (Integer.parseInt(args[1])) : 8;
    MazeGenerator maze = new MazeGenerator(x, y);
    maze.display();
  }
  */
}

private enum DIR {
    N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
    private final int bit;
    private final int dx;
    private final int dy;
    private DIR opposite;
 
    // use the static initializer to resolve forward references
    static {
      N.opposite = S;
      S.opposite = N;
      E.opposite = W;
      W.opposite = E;
    }
 
    private DIR(int bit, int dx, int dy) {
      this.bit = bit;
      this.dx = dx;
      this.dy = dy;
    }
  };