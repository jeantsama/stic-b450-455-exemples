

public class Perso
{
int x;
int y;
int orientation;
PImage[] sprite;

  public Perso()
  {
    x = 0;
    y = 0;
    orientation = 2;
    
    sprite = new PImage[4];
    sprite[3] =loadImage("Left.png");
    sprite[1] =loadImage("Right.png");
    sprite[2] =loadImage("Front.png");
    sprite[0] =loadImage("Back.png");

  }
public int getX(){return x;}
public int getY(){return y;}

  
public boolean isAWallOnRigthHand()
{
  println("isAWallOnRigthHand]]::",orientation, this.x,this.y);
  if(orientation%4 == 0)return maze.isEastWall(this.x,this.y);
  if(orientation%4 == 1)return maze.isSouthWall(this.x,this.y);
  if(orientation%4 == 2)return maze.isWestWall(this.x,this.y);
  if(orientation%4 == 3)return maze.isNorthWall(this.x,this.y);  
  return false;
}

public boolean isWallAhead()
{
  println("isWallAhead]]::",orientation, this.x,this.y);
  if(orientation%4 == 0)return maze.isNorthWall(this.x,this.y);
  if(orientation%4 == 1)return maze.isEastWall(this.x,this.y);
  if(orientation%4 == 2)return maze.isSouthWall(this.x,this.y);
  if(orientation%4 == 3)return maze.isWestWall(this.x,this.y);  

  return false;
}


public int  getOrientation(){return orientation;}

public void turnLeft()
{
  orientation+=3;
  println("turnLeft]] new orientation",orientation);
}

public void turnRight()
{
  orientation++;
  println("turnRight]] new orientation",orientation);
}

public void OneStep()
{
  if(orientation%4 == 0)y--;
  if(orientation%4 == 1)x++;
  if(orientation%4 == 2)y++;
  if(orientation%4 == 3)x--;
}
void display()
{
  display(255);
}

void display(int t)
  {
      tint(255, t); 
      fill(232,223,123);
      image(sprite[orientation%4],x*case_size,y*case_size,case_size, case_size);
 
  }

}