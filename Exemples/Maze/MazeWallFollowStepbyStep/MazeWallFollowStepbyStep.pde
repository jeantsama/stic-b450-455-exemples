/*

Algorithme Wall Follower en processing

Auteurs : Julien Jabon & Joël Goossens

Février 2017

*/

/*****************************GLOBAL VARIABLE***************************************/
MazeGenerator maze;                     // Variable contenant le labyrinthe
Perso p;                                // Variable contenant le personnage
final int maze_size = 25;                     // Nombre de cases par
                                        //hauteur et longeur
final int screen_size = 800;                  // Taille de l'écran
final int case_size = screen_size/maze_size;  // Rapport nombre de cases/ecran
int stateturn = 0;                      // état initial, nous avons un mur à notre droite

/*****************************SETUP***************************************/
void setup(){
  frameRate(60);                          //nombre de frame par seconde
  size(800,800);         // taille de la fenetre
  maze = new MazeGenerator(maze_size);    // Création et génération du maze  (maze carré).
  maze.ASCIIdisplay();                    //Affichage du labyrinthe sur la console.
  p = new Perso();                        //Création du personnage
}

/*****************************DRAW***************************************/
void draw(){
  maze.display();                       //On dessine le labyrinthe à l'écran
  p.display();                          //On dessine le personnage
}

void keyPressed(){
    handFollow();
}
/*****************************handFollow***************************************/
// Nous allons utiliser 4 états.
//
//+---+---+---+
//|        => |
//+---+---+   +
//|   |       |
//+   +   +---+
//
//+---+---+---+
//|        => |
//+---+---+   +
//|       |   |
// Dans ce cas là, il faut retrouver le mur en tournant.
/********************************************************************************/
void handFollow(){
    if (p.getX() == maze.endX() && p.getY() == maze.endY())
        return;
        // Si on a trouvé la fin, on ne fait plus rien.
    if(stateturn == 0){
        // State 0 : le cas le plus simple. Peut se résoudre en une étape. Nous avons un mur à notre droite
        if(p.isAWallOnRigthHand() && !p.isWallAhead()){
            // Le personnage voit un mur à droite et pas de mur devant
            println("no WallAhead and WallOnRigthHand");
            p.OneStep(); // Le personnage avance d'un pas
            // on reste l'état 0
        } else if(p.isAWallOnRigthHand() && p.isWallAhead()){
            // un mur a droite et un mur devant
            println("WallAhead and WallOnRigthHand");
            p.turnLeft();
            // Le personnage tourne seulement d'un cran à gauche.
            // On reste dans l'état 0
        } else if (!p.isAWallOnRigthHand()){
            // Le personnage n'a plus de mur à droite
            println("Oh my god no walls anymore");
            p.turnRight();
            // Il tourne a droite et paste la machine à état en state 1
            stateturn = 1;
        }
    } else if (stateturn == 1){
        // état = 1 : il n'ya plus de mur à main droite.
        p.OneStep(); // Le personnage avance d'un pas
        if (!p.isAWallOnRigthHand()){
            // Le personnage regarde si il y a un mur
            stateturn = 2; //si pas de mur on continue a tourner
        }
        else {
            stateturn = 0; // S'il y a un mur, on revient en état 0.
        }
    } else if (stateturn == 2){
            p.turnRight(); //  Toujours pas de mur, le personnage tourne et passe dans l'état 3.
            stateturn = 3;
    } else if (stateturn == 3){
        p.OneStep(); // La il y a obligatoirement un mur.
        stateturn = 0;
    }
}
