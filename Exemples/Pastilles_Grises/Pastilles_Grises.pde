// Dégradé de 255 lignes avec des pastilles grises
size (600,600);
background (255);
int i = 0;
float mycolor = 0;
while (i <= 600){
  int j = 0;
  while (j <= 600){
    noStroke();
    fill(mycolor);
    ellipse(j,i,5,5);     
    j = j+10;
  }
  i = i+10; 
  mycolor = mycolor+3;
}