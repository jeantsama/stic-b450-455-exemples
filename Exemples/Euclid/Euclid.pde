/*
   Euclid's algorithm
   Author: Joël Goossens
   June 2016
*/

int cornerX, cornerY, stopX, stopY;
int myColor;
int w,h;
int scaleX,scaleY;

void setup() {
  size(450, 450);
  frameRate(1);
  myColor=170;
  w=76; 
  h=34;
  if (w>=h){
    scaleX=width/w;
    scaleY=scaleX;
  } 
  else{
    scaleY=height/h;
    scaleX=scaleY;
  }
  cornerX=0;
  cornerY=0;
  stopX=cornerX+w;
  stopY=cornerY+h;
  noFill();
  rect(cornerX*scaleY,cornerY*scaleY,w*scaleX,h*scaleY);
}

void draw() {
  if (w==h){
    println(w);
    noLoop();
    return; //we are done
  }
  if (w<=h){
    fill(color(myColor, 180, 360));
    rect(cornerX*scaleX,cornerY*scaleY,w*scaleX,w*scaleY);  
    myColor = (myColor + 40) % 256;
    cornerY=cornerY+w;
    h=h-w;
  }
  else{
    fill(color(myColor, 180, 360));
    rect(cornerX*scaleX,cornerY*scaleY,h*scaleX,h*scaleY);
    myColor = (myColor + 40) % 256;
    cornerX=cornerX+h;
    w=w-h;
  }
}
