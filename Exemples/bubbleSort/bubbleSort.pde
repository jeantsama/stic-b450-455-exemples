/*
   Bubble Sort
*/

int num_items = 20;
int minval = 1;
int maxval = 99;
int visitor, tail;
int posMax;

int[] unsorted;
int[] bubblesorted;
int[] selectionSorted;

void setup() {
    size(450, 400);
    colorMode(HSB,360);
    textFont(createFont("SansSerif", 12, true));
    noStroke();

    frameRate(12);
    tail = num_items-1;

    unsorted = new int[num_items];
    for (int i=0; i<num_items; i++) {
        unsorted[i] = (int) random(minval, maxval);
    }

    selectionSorted = new int[num_items];
    arrayCopy(unsorted, selectionSorted);

    bubblesorted = new int[num_items];
    arrayCopy(unsorted, bubblesorted);

    visitor = 0;
    posMax=0;
}


void draw() {
    fill(360, 200);
    rect(0, 0, width, height);

    fill(0);
    textSize(12);
    text("Right-click to generate a new random array.", 30,370);

    visualize("Unsorted",      unsorted,       30,50,  20, false);
    //visualize("Selection",  selectionSorted,    30,150, 20, true);
    //visualize("Selection sort",   selectionSorted, 30,150, 20, true);
    //selectionMax_one_step();
    //selectionSort_one_step();
    visualize("Bubblesort",bubblesorted,30,150, 20,true);
    //saveFrame("/Users/joelgoossens/Downloads/movie/frames/####.png");
    //bubblesort_one_pass();
    bubblesort_one_step();
}

void mousePressed() {
    if (mouseButton == RIGHT)
        setup();
}

void bubblesort_one_pass() {
    if (visitor==tail)
        // We're done
        return;

    if (bubblesorted[visitor] > bubblesorted[visitor+1]) {
        int swap = bubblesorted[visitor];
        bubblesorted[visitor] = bubblesorted[visitor+1];
        bubblesorted[visitor+1] = swap;
    }
    
       visitor++;     
}

void bubblesort_one_step() {
    if (tail == 0)
        // We're done
        return;

    if (bubblesorted[visitor] > bubblesorted[visitor+1]) {
        int swap = bubblesorted[visitor];
        bubblesorted[visitor] = bubblesorted[visitor+1];
        bubblesorted[visitor+1] = swap;
    }
    else {
        // Skip already sorted items
        visitor++;
        if (visitor == tail) {
            visitor = 0;
            tail--;
        }
    }
}

void selectionSort_one_step(){
  if (tail == 0)
    // We're done
    return;
  if (visitor == tail+1){
    //swap
    int swap = selectionSorted[posMax];
    selectionSorted[posMax] = selectionSorted[tail];
    selectionSorted[tail] = swap;
    visitor = 0; posMax=0;
    tail--;
    }   
    else{
      if (selectionSorted[posMax]<selectionSorted[visitor]){
        posMax = visitor;
        }
        visitor++;
    }
}

void selectionMax_one_step() {
    if (visitor == num_items)
      return; // We are done
    if (selectionSorted[posMax]<selectionSorted[visitor])
      posMax = visitor;
    visitor++;
}

void visualize(String caption,
               int[] values,
               float x, float y, float xstep,
               boolean showCursor) {
    fill(0);
    textSize(14);
    text(caption, x, y);

    textSize(10);
    for (int i=0; i < values.length; i++) {
        int v = values[i];

        if (i == visitor && showCursor) {
            fill(0);
            noStroke();
            rect(x+(xstep*i), y+3, 5, 5);
        }


        fill(0);
        text(str(v), x+(xstep*i), y+25);

        float w = map(v, minval, maxval, 3, 15);
        float h = map(v, minval, maxval, 10, 50);

        fill( valueToColor(v) );
        rect(x+(xstep*i), y+30, w, h);
    }
}

// Convert a  [minval, maxval] value to a color
int valueToColor(int value) {
    float hue = norm(value, minval, maxval) * 360;
    return color(hue, 180, 360);
}

void bubbleSort(int A[]){
  int t, droite, i;
  for(droite=A.length-1;droite>0;--droite)
    for(i=0;i<droite;++i)
      if (A[i]>A[i+1]){
      t=A[i];A[i]=A[i+1];A[i+1]=t;   
    }  
}